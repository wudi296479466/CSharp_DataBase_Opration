﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sharps
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Database db = new Database();
                db.init();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.ReadKey();
        }
    }
}
