﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace Sharps
{
    public sealed class SqlDatabase : IDatabase
    {
        private SqlConnection con;
        private static SqlDatabase db;

        /// <summary>
        /// 实现链接 
        /// </summary>
        /// <param name="conString">Sql链接字符串</param>
        public void Connect(string conString)
        {
            try
            {
                con = new SqlConnection(conString);
            }
            catch
            {
                throw ConnectException.getInstanse();
            }
        }

        /// <summary>
        /// 获取此类的实例 (工厂模式)
        /// </summary>
        /// <returns></returns>
        public static SqlDatabase getInstanse()
        {
            if (db == null)
            {
                db = new SqlDatabase();

            }
            return db;
        }

        /// <summary>
        /// 实现查询
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public System.Data.DataTable Query(string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand com = new SqlCommand(sql, con);
                SqlDataAdapter adapter = new SqlDataAdapter(com);
                adapter.Fill(dt);
            }
            catch (Exception)
            {

            }
            return dt;
        }
        /// <summary>
        /// 实现关闭链接
        /// </summary>
        public void Close()
        {
            try
            {
                con.Close();
            }
            catch
            {
            }
        }
        /// <summary>
        /// 执行SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public int Excute(string sql)
        {
            int p = -1;
            try
            {
                p = new SqlCommand(sql, con).ExecuteNonQuery();
            }
            catch
            {
            }
            return p;
        }
    }
}
